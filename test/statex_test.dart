import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:vfx_flutter_common/getx_helpers.dart';
import 'package:vfx_flutter_common/utils.dart';

import 'common/common.dart';

void main() {
  test('args test', () async {
    print('${testDelimiter()}: args test');
    {
      Get.put(_Controller());
      await delayMilli(1);
      final c = Get.find<_Controller>();
      expect(c.args, equals(JsonMap()));
    }
    {
      final map = {'a': 'b'};
      Get.put(_Controller());
      await delayMilli(1);
      final c = Get.find<_Controller>()..args = map;
      expect(c.args, equals(map));
    }
    {
      Get.put(_Controller()..args = {'a': 'b'});
      await delayMilli(1);
      final map = {'c': 'd'};
      final c = Get.find<_Controller>()..args = map;
      expect(c.args, equals(map));
    }
  });
}

class _Controller extends StatexController {}
