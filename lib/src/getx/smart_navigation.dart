import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pedantic/pedantic.dart';

import 'package:vfx_flutter_common/src/utils/funcs.dart';

typedef SmartNavigationClose<T> = Function(T, {Map<String, dynamic>? args});

typedef SubWidgetBuilder = Widget Function();

/// Wrapper for Get.x methods with most frequently used arguments
typedef PageRunner = Future<dynamic>? Function(
  GetPageBuilder?, {
  Bindings binding,
  Transition transition,
  Duration duration,
});

/// Simple and effective mixin for [GetxController] and [GetxService]
/// to make navigation
mixin SmartNavigationMixin<T> on DisposableInterface {
  /// Switches global log for this
  static var logging = false;

  /// Keeps client's callback
  SmartNavigationClose<T>? _closeCallback;

  /// Flag for auto delete [T] from memory when closing
  bool _autoDelete = true;

  /// Builder for Page via Get.x
  GetPageBuilder? get defaultGetPageBuilder => null;

  /// Placer in Host
  var subWidgetPlacer$ = Rx<Widget>(Container());

  /// Sub-view for Host's [subWidgetPlacer$]
  SubWidgetBuilder? get defaultSubWidgetBuilder => null;

  ///
  Future toPage({
    SmartNavigationClose<T>? onClose,
    GetPageBuilder? pageBuilder,
    bool? autoDelete,
    Transition? transition,
    Duration? duration,
    Map<String, dynamic>? args,
  }) async {
    _log('Get.to(...)');
    unawaited(_pageNavigate(
      Get.to,
      onClose: onClose,
      pageBuilder: pageBuilder,
      autoDelete: autoDelete,
      transition: transition,
      duration: duration,
      args: beforeNavigate(args: args),
    ));
  }

  ///
  Future offPage({
    SmartNavigationClose<T>? onClose,
    GetPageBuilder? pageBuilder,
    bool? autoDelete,
    Transition? transition,
    Duration? duration,
    Map<String, dynamic>? args,
  }) async {
    _log('Get.off(...)');
    unawaited(_pageNavigate(
      Get.off,
      onClose: onClose,
      pageBuilder: pageBuilder,
      autoDelete: autoDelete,
      transition: transition,
      duration: duration,
      args: beforeNavigate(args: args),
    ));
  }

  ///
  Future offAllPage({
    SmartNavigationClose<T>? onClose,
    GetPageBuilder? pageBuilder,
    bool? autoDelete,
    Transition? transition,
    Duration? duration,
    Map<String, dynamic>? args,
  }) async {
    _log('Get.offAll(...)');
    unawaited(_pageNavigate(
      Get.offAll,
      onClose: onClose,
      pageBuilder: pageBuilder,
      autoDelete: autoDelete,
      transition: transition,
      duration: duration,
      args: beforeNavigate(args: args),
    ));
  }

  /// Deep navigation when [subWidgetBuilder]'s object
  /// places to [subWidgetPlacer$]
  void subWidgetNavigate({
    @required Rx<Widget>? subWidgetPlacer$,
    @required SmartNavigationClose<T>? onClose,
    SubWidgetBuilder? subWidgetBuilder,
    bool? autoDelete,
    Map<String, dynamic>? args,
  }) {
    // It prevents from mixing without pointing to real type
    if (this is! SmartNavigationMixin<DisposableInterface>) {
      throw Exception('possibly you use not a template SmartNavigationMixin '
          ' instead of SmartNavigationMixin<$runtimeType> ');
    }

    if (subWidgetPlacer$ == null) {
      throw Exception('when one do subWidgetNavigate(), '
          'subWidgetPlacer\$ must not be null');
    }
    if ((subWidgetBuilder ?? defaultSubWidgetBuilder) == null) {
      throw Exception('when one do subWidgetNavigate(), '
          'subWidgetBuilder or defaultSubWidgetBuilder '
          'must not be null');
    }
    _closeCallback = onClose;
    // If not set we set it automatically to false for [GetxService]
    // or to true otherwise (mean [GetxController])
    _autoDelete = autoDelete ?? this is! GetxService;
    this.subWidgetPlacer$ = subWidgetPlacer$;
    //
    final args2 = beforeNavigate(args: args);
    //
    this.subWidgetPlacer$(
        (subWidgetBuilder ?? defaultSubWidgetBuilder)?.call());
    //
    Future.delayed(Duration.zero).then((_) => afterNavigate(args: args2));
  }

  ///  (r) [Template Method]
  ///  When need analyze arguments before launch `..Page(..)`
  Map<String, dynamic>? beforeNavigate({Map<String, dynamic>? args}) => args;

  // TODO(vvk): Use it functionality !!!For test!
  /// When something should start after main navigation
  Future afterNavigate({Map<String, dynamic>? args}) async {}

  /// [result] helps cooperate with [WillPopScope.onWillPop]
  /// Когда речь идет о закрытии суба, то не всегда стоит
  /// возвращать [true], чтобы не закрыть случайно [subWidgetPlacer$]
  Future<bool> close({bool result = false, Map<String, dynamic>? args}) async {
    if (logging) {
      debugPrint('$now: [SNM]: close $runtimeType');
    }
    _closeCallback?.call(this as T, args: args);

    /*
    TODO(vvk): 22.06.2021: надо сделать по _autoDelete
     и переименовать в AltNavigation

    */
    Future.delayed(Duration.zero).then((_) async {
      if (Get.isRegistered<T>()) {
        return await Get.delete<T>();
      }
    });
    return result;
  }

  ///
  Future _pageNavigate(
    PageRunner? pageRunner, {
    SmartNavigationClose<T>? onClose,
    GetPageBuilder? pageBuilder,
    bool? autoDelete,
    Transition? transition,
    Duration? duration,
    Map<String, dynamic>? args,
  }) async {
    // It prevents from mixing without pointing to real type
    if (this is! SmartNavigationMixin<DisposableInterface>) {
      throw Exception('possibly you use wrong SmartNavigationMixin<???> '
          ' instead of SmartNavigationMixin<$runtimeType> ');
    }

    if ((pageBuilder ?? defaultGetPageBuilder) == null) {
      throw Exception('when one do pageNavigate, pageBuilder '
          'or defaultPageBuilder must not be null');
    }
    _closeCallback = onClose;
    // If not set we set it automatically to false for [GetxService]
    // or to true otherwise (mean [GetxController])
    _autoDelete = autoDelete ?? this is! GetxService;
    final builder = pageBuilder ?? defaultGetPageBuilder;
    pageRunner?.call(
      builder,
      binding: BindingsBuilder(() => this),
      transition: transition ?? Transition.fade,
      duration: duration ?? const Duration(milliseconds: 350),
    );
    Future.delayed(Duration.zero).then((_) => afterNavigate(args: args));
  }

  ///
  void _log(String method) {
    if (logging) {
      debugPrint('$now: [SNM]: open $runtimeType: $method');
    }
  }
}

/// Useful methods
abstract class SmartNavigation<S extends DisposableInterface> {
  /// Used for smart deleting previous instance before adding new
  static Future<S> put<S>(S instance,
      {String? tag, bool permanent = false}) async {
    return _deleteGet<S>().then((value) {
      if (!value) {
        debugPrint(
            '$now: SmartNavigation.put: ERROR of deleting ${_typeOf<S>()}');
      }
      final s = Get.put<S>(instance, tag: tag, permanent: permanent);
      return s;
    });
  }

  static Future<bool> _deleteGet<S>({String? tag, bool force = false}) async {
    if (Get.isRegistered<S>(tag: tag)) {
      return await Get.delete<S>(tag: tag, force: force);
    }
    return true;
  }

  static Type _typeOf<T>() => T;
}
