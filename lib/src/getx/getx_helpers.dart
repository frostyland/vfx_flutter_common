import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart' as rx;

import '../utils/types.dart';

/// Общее поведение для сервиса
/// [subscriptions] позволяет управлять временем жизни подписок
abstract class GetxServiceProxy extends GetxService {
  final subscriptions = rx.CompositeSubscription();

  @mustCallSuper
  @override
  void onClose() {
    subscriptions.clear();
    super.onClose();
  }
}

/// Общее поведение для контроллера
/// [subscriptions] позволяет управлять временем жизни подписок
abstract class GetxControllerProxy extends GetxController {
  final subscriptions = rx.CompositeSubscription();

  @mustCallSuper
  @override
  void onClose() {
    subscriptions.clear();
    super.onClose();
  }
}

/// Работает аналогично GetView в Get (https://pub.dev/packages/get)
/// (Симулятор, Sim), только добавляет для удобства [tag]
/// в параметры конструктора
///
abstract class GetViewSim<T> extends StatelessWidget {
  const GetViewSim({this.tag, Key? key}) : super(key: key);

  final String? tag;

  /// Для педантов
  T get controller => GetInstance().find<T>(tag: tag)!;

  /// Для ленивых ))
  T get c => GetInstance().find<T>(tag: tag)!;

  @override
  Widget build(BuildContext context);
}

////////////////////////////////////////////////////////////////////////////////

///
abstract class AppSnackbar {
  static void stdOrAlarm(bool isAlarm, String message,
      {String? title, Duration? duration}) {
    if (isAlarm) {
      alarm(message, title: title, duration: duration);
    } else {
      std(message, title: title, duration: duration);
    }
  }

  static void std(String message, {String? title, Duration? duration}) {
    Get.rawSnackbar(
      title: title,
      message: message,
      duration: duration ?? Duration(seconds: 2),
      snackPosition: SnackPosition.BOTTOM,
      borderRadius: 0,
      barBlur: 0,
      backgroundColor: Colors.black,
      margin: EdgeInsets.all(0),
      dismissDirection: DismissDirection.horizontal,
    );
  }

  /// With red color
  static void alarm(String message, {String? title, Duration? duration}) {
    Get.rawSnackbar(
      title: title,
      message: message,
      snackPosition: SnackPosition.BOTTOM,
      duration: duration ?? Duration(seconds: 2),
      borderRadius: 0,
      barBlur: 0,
      backgroundColor: Colors.red,
      margin: EdgeInsets.all(0),
      dismissDirection: DismissDirection.vertical,
    );
  }
}

/// Пример, как можно использовать [RouteArgs] для анализа данных
/// ```
///   @override
///   Future onReady() async {
///     super.onReady();
///
///     var duration = Duration(milliseconds: 300);
///     var transition = Transition.upToDown;
///
///     if(Get.arguments is RouteArgs) {
///       final ra = (Get.arguments as RouteArgs);
///       final sender = ra.sender;
///       if(sender is! AuthService){
///         transition = Transition.size;
///         duration = Duration(milliseconds: 800);
///       }
///     }
///
///     Get.to(
///       () => SettingsPage(),
///       arguments: RouteArgs(sender: this, willPopCallback: tryClose),
///       transition: transition,
///       duration: duration,
///     );
///   }
/// ```
class RouteArgs extends Equatable {
  const RouteArgs({
    required this.sender,
    this.senderPage,
    this.reason,
    this.willPopCallback,
    this.senderCallback,
    this.transition,
    this.duration,
    this.curve,
    this.predicate,
    this.opaque = false,
    this.binding,
    this.popGesture,
    this.id,
    this.fullscreenDialog = false,
    this.preventDuplicates = true,
    this.arguments = const <String, dynamic>{},
  });

  static const invalid = RouteArgs(sender: null);

  final sender;
  final String? senderPage;
  final reason;
  final WillPopCallback? willPopCallback;
  final SenderCallback? senderCallback;
  final Transition? transition;
  final Duration? duration;
  final Curve? curve;
  final RoutePredicate? predicate;
  final bool opaque;
  final Bindings? binding;
  final bool? popGesture;
  final int? id;
  final bool fullscreenDialog;
  final bool preventDuplicates;
  final JsonMap arguments;

  @override
  List<Object?> get props =>
      [sender, senderPage, willPopCallback, senderCallback, arguments];
}

RouteArgs routeArgs({dynamic arguments}) {
  arguments ??= Get.arguments;
  if (arguments is! RouteArgs) {
    return RouteArgs.invalid;
  }
  return arguments as RouteArgs;
}

Map<String, dynamic> get safeArguments {
  final arguments = Get.arguments;
  if (arguments is! RouteArgs) {
    return {};
  }
  return (arguments as RouteArgs).arguments;
}

typedef SenderCallback = Future<bool> Function(JsonMap args);

/// Хелпер для передачи аргументов в стандарнтые методы [Get]
/// Задача клиента - передать фабрику вьюхи и аргументы.
/// Методы распарсят аргументы в параметры стандартных вызовов.
/// --
/// Это имеет смысл когда в аргументы передаются какие-то нестандартные
/// параметры навигации - Transition, Duration, etc.
/// в противном случае можно пользоваться именованными вызовами,
/// передавая туда напрмере просто сендер, [willPopCallback].
///
///
/// ```
/// Пример:
///
/// 1. Ждем некие уже готовые аргументы, и если их нет
///
///   // Когда аргументов нет, значит это старт и мы сносим все
///   // и навешиваем только колбек
///   if(args == null){
///     Get.offAllNamed(
///       RouteNames.settings,
///       arguments: RouteArgs(
///         sender: this,
///         willPopCallback: finishSecurity,
///       ),
///     );
///     return;
///   }
///
///   // Когда есть аргументы, навешиваем их сверху на вызов нужной вьюхи.
///   AppGet.to(() => SettingsPage(), args);
/// }
///
/// А вот как их готовили в некоем месте, где следует изменить [Transition]
///
///     Get.find<MainViewService>().startSecurity(
//       RouteArgs(
//         sender: this,
//         transition: Transition.leftToRight,
//         // duration: Duration(seconds: 1),
//       ),
//     );
///
/// ```
///
abstract class GetProxy {
  static to<T>(InstanceBuilderCallback<T> builder, RouteArgs args) {
    Get.to(
      builder,
      opaque: args.opaque,
      transition: args.transition ?? Get.defaultTransition,
      curve: args.curve ?? Get.defaultDialogTransitionCurve,
      duration: args.duration ?? Get.defaultTransitionDuration,
      id: args.id,
      fullscreenDialog: args.fullscreenDialog,
      arguments: args,
      binding: args.binding,
      preventDuplicates: args.preventDuplicates,
      popGesture: args.popGesture ?? Get.defaultPopGesture,
    );
  }

  static off<T>(InstanceBuilderCallback<T> builder, RouteArgs args) {
    Get.off(
      builder,
      opaque: args.opaque,
      transition: args.transition ?? Get.defaultTransition,
      curve: args.curve ?? Get.defaultDialogTransitionCurve,
      duration: args.duration ?? Get.defaultTransitionDuration,
      id: args.id,
      fullscreenDialog: args.fullscreenDialog,
      arguments: args,
      binding: args.binding,
      preventDuplicates: args.preventDuplicates,
      popGesture: args.popGesture ?? Get.defaultPopGesture,
    );
  }

  static offAll<T>(InstanceBuilderCallback<T> builder, RouteArgs args) {
    Get.offAll(
      builder,
      predicate: args.predicate,
      opaque: args.opaque,
      transition: args.transition ?? Get.defaultTransition,
      curve: args.curve ?? Get.defaultDialogTransitionCurve,
      duration: args.duration ?? Get.defaultTransitionDuration,
      id: args.id,
      fullscreenDialog: args.fullscreenDialog,
      arguments: args,
      binding: args.binding,
      popGesture: args.popGesture ?? Get.defaultPopGesture,
    );
  }
}
