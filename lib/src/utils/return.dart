import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

class Return<R, D> extends Equatable {
  final R result;
  final D? data;
  final String? description;

  Return({
    required this.result,
    this.data,
    this.description,
  });

  @override
  List<Object?> get props => [result, data, description];
}

abstract class Boolean<D> extends Return<bool, D> {
  bool get isTrue;
  Boolean(bool result, {D? data, String? description})
      : super(result: result, data: data, description: description);
}

class True<D> extends Boolean<D> {
  @override
  bool get isTrue => true;
  True({D? data, String? description})
      : super(true, data: data, description: description);
}

class False<D> extends Boolean<D> {
  @override
  bool get isTrue => false;
  False({D? data, String? description})
      : super(false, data: data, description: description);
}
