import 'package:collection/collection.dart';
// Handy mini functions and properties

/// Delay in microseconds
Future delayMicro(int value) async =>
    Future<int>.delayed(Duration(microseconds: value));

/// Delay in milliseconds
Future<int> delayMilli(int value) async =>
    Future<int>.delayed(Duration(milliseconds: value), () => Future.value(1));

/// Delay in seconds
Future delaySec(int value) async =>
    Future<int>.delayed(Duration(seconds: value));

/// Shortcut for [DateTime.now]
DateTime get now => DateTime.now();

/// Shortens deep equality for collections.
bool smartEq<T>(T data1, T data2) {
  if (data1 is Iterable) {
    return const DeepCollectionEquality().equals(data1, data2);
  } else {
    return data1 == data2;
  }
}
